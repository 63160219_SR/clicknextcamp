interface Menu {
    name: string 
    subMenu: SubMenu[]
  }
   
  interface SubMenu {
    name: string
  }
   
  const menus: Menu[] = [
    {
      name: 'Home',
      subMenu: [],
    },
    {
      name: 'About',
      subMenu: [
        {
          name: 'Company',
        },
        {
          name: 'Team',
        },
      ],
    },
    {
      name: 'Products',
      subMenu: [
        {
          name: 'Electronics',
        },
        {
          name: 'Clothing',
        },
        {
          name: 'Accessories',
        },
      ],
    },
    {
      name: 'Services',
      subMenu: [],
    },
    {
      name: 'Contact',
      subMenu: [
        {
          name: 'Phone',
        },
      ],
    },
    {
      name: 'Blog',
      subMenu: [],
    },
    {
      name: 'Gallery',
      subMenu: [
        {
          name: 'Photos',
        },
        {
          name: 'Videos',
        },
        {
          name: 'Events',
        },
      ],
    },
    {
      name: 'FAQ',
      subMenu: [],
    },
    {
      name: 'Downloads',
      subMenu: [
        {
          name: 'Documents',
        },
        {
          name: 'Software',
        },
      ],
    },
    {
      name: 'Support',
      subMenu: [
        {
          name: 'Help Center',
        },
        {
          name: 'Contact Us',
        },
        {
          name: 'Knowledge Base',
        },
      ],
    },
  ];

  const menuName = document.getElementById("exercise") as HTMLDivElement
  const MenuName = document.createElement("ul");
  for(const name of menus){
    const names = document.createElement("li");
    const ULName = document.createTextNode(name.name)
    if(names.appendChild(ULName)){

        if(name.subMenu.length>0){
            const SubMenu = document.createElement("ul")
            for(const subname of name.subMenu){
                const submenu = document.createElement("li")
                const LIName = document.createTextNode(subname.name)
                submenu.appendChild(LIName)
                SubMenu.appendChild(submenu)
            }
            names.appendChild(SubMenu)
        }

    }
    MenuName.appendChild(names)
  }
  menuName.appendChild(MenuName)

  export {};